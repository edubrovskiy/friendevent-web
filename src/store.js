import Vue from 'vue'
import Vuex from 'vuex'
import VueCookie from 'vue-cookie'

Vue.use(Vuex)
Vue.use(VueCookie)

const SESSION_ID = 'sessionid';

export default new Vuex.Store({
  state: {
    authenticated: Vue.cookie.get(SESSION_ID) != null,
  },
  mutations: {
    login(state) {
      state.authenticated = true
    },
    logout(state) {
      state.authenticated = false
      Vue.cookie.delete(SESSION_ID);
    }
  }
})
