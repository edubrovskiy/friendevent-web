import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import store from './store.js'
import VueResource from 'vue-resource'
import VueCookie from 'vue-cookie'
import GoogleMapsLoader from 'google-maps'

Vue.use(VueResource)
Vue.use(VueCookie)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

Vue.http.options.root = 'https://friendevent.edubrovskiy.ru:9090/api/v1'
Vue.http.options.credentials = true

GoogleMapsLoader.KEY = 'AIzaSyAeWIwmmZ_maRGrHqouUmuF3OhU29i-E6s';