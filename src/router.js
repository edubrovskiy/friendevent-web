import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/Login.vue'
import SignUp from './components/SignUp.vue'
import EditAccount from './components/EditAccount.vue'
import About from './components/About.vue'
import NotFound from './components/NotFound.vue'
import UnexpectedError from './components/UnexpectedError.vue'
import Events from './components/Events.vue'
import CreateEvent from './components/CreateEvent.vue'
import EventDetails from './components/EventDetails.vue'
import UserDetails from './components/UserDetails.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: About
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/signup',
      component: SignUp
    },
    {
      path: '/account',
      component: EditAccount
    },
    {
      path: '/events',
      component: Events
    },
    {
      path: '/events/new',
      component: CreateEvent
    },
    {
      path: '/events/:id',
      component: EventDetails
    },
    {
      path: '/users/:id',
      component: UserDetails
    },
    {
      path: '/unexpectederror',
      component: UnexpectedError
    },
    {
      path: '*',
      component: NotFound
    }
  ]
})
